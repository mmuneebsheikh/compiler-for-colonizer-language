﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LexcalGui
{
    public partial class txtTokens : Form
    {
        CFG c = new CFG();
        int lineNo = 1;
        string inp;
        string temp;


        bool str = false, stChar = false, ROtoken = false, DivMulMod = false, sign = false, invalid = false;
        bool terminator = false, doubleColon = false, colon = false, opRdBrak = false, clRdBrak = false, isNewLine = false;
        bool areAllTokens = true;


        bool opSqBrak = false, clSqBrak = false, opCrBrak = false, clCrBrak = false, addSub = false, invalid2 = false;
        bool ext = false, ins = false, onlyDot = false, AOtoken = false, isComma = false, singleComm = false, multiComm = false;
        string strADDSUB, strRO, strDIV, strInv, strInv2, strSign;
        int i = 0;


        public txtTokens()
        {
            InitializeComponent();
        }


        //Tokens*************************************

        void commaToken(string inp, int lineNo) //INSERTION Token
        {
            Result.Text += "(" + inp + ",,," + lineNo + ")" + Environment.NewLine;
        }
        void insertionToken(string inp, int lineNo) //INSERTION Token
        {
            Result.Text += "(" + inp + ",<<, " + lineNo + ")" + Environment.NewLine;
        }
        void extractionToken(string inp, int lineNo) //EXTRACTION Token
        {
            Result.Text += "(" + inp + ",>>," + lineNo + ")" + Environment.NewLine;
        }
        void intToken(string inp, int lineNo) //INT Token
        {
            Result.Text += "(" + inp + ",int," + lineNo + ")" + Environment.NewLine;
        }
        void charToken(string inp, int lineNo) //char Token
        {
            StringBuilder str = new StringBuilder(inp.Substring(1, inp.Length - 2));

            for (int i = 0; i < str.Length; i++)
            {

                if (str[i] == '\\')
                {
                    if (str[i + 1] != '\'' && str[i + 1] != '\\')
                    {

                    }
                    else
                    {
                        str = str.Remove(i, 1);
                        i += 2;
                    }

                }
            }

            Result.Text += "(" + str + ",char," + lineNo + ")" + Environment.NewLine;
        }
        void floatToken(string inp, int lineNo) //float Token
        {
            Result.Text += "(" + inp + ",float," + lineNo + ")" + Environment.NewLine;
        }
        void stringToken(string inp, int lineNo) //string Token
        {
            //inp = inp.Substring(1, inp.Length - 2);
            StringBuilder str = new StringBuilder(inp.Substring(1, inp.Length - 2));
            for (int i = 0; i < str.Length; i++)
            {

                if (str[i] == '\\')
                {
                    if (str[i + 1] != '"' && str[i+1] != '\\')
                    {

                    }
                    else
                    {
                        str = str.Remove(i, 1);
                        i += 2;
                    }

                }
            }
            Result.Text += "(" + str + ",string," + lineNo + ")" + Environment.NewLine;
        }
        void IdentifierToken(string inp, int lineNo) //identifier Token
        {
            Result.Text += "(" + inp + ",ID," + lineNo + ")" + Environment.NewLine;
        }
        void KeyWordToken(string inp, int lineNo) //KW token
        {
            string[] DT = { "int", "float",  "char", "bool", "string" };
            string[] IND = { "cout","cin","var", "till", "from", "func", "class", "this", "break", "loop", "try", "catch", "childOf", "static", "namespace", "return", "$include", "std", "using", "array", "cout", "cin", "shift", "in", "if", "else", "new", "unit", "default","iostream", "Enter" };
            string[] AM = { "private", "public", "protected" };
            string[] LO = { "NOT", "AND", "OR" };
            string[] BOOL = { "true", "false" };
            string[] IncDec = {"INC","DEC"};

            //check for LO
            for (int i = 0; i < 3; i++)
            {
                if (inp == LO[i])
                {
                    if (LO[i] == "NOT")
                    {
                        Result.Text += "(" + inp + ",NOT," + lineNo + ")" + Environment.NewLine;
                    }
                    else
                    {
                        Result.Text += "(" + inp + ",LO," + lineNo + ")" + Environment.NewLine;
                    }

                    return;
                }
            }

            //check for INC DEC
            for (int i = 0; i < IncDec.Length; i++)
            {
                if (inp == IncDec[i])
                {
                    Result.Text += "(" + inp + ",INCDEC," + lineNo + ")" + Environment.NewLine;
                    return;
                }
            }
            //Check for DT
            for (int i = 0; i < DT.Length; i++)
            {
                if (inp == DT[i])
                {
                    Result.Text += "(" + inp + ",DT," + lineNo + ")" + Environment.NewLine;
                    return;
                }
            }
            //check for Bool
            for (int i = 0; i < BOOL.Length; i++)
            {
                if (inp == BOOL[i])
                {
                    Result.Text += "(" + inp + ",bool," + lineNo + ")" + Environment.NewLine;
                    return;
                }
            }
            //check for AM
            for (int i = 0; i < AM.Length; i++)
            {
                if (inp == AM[i])
                {
                    Result.Text += "(" + inp + ",AM," + lineNo + ")" + Environment.NewLine;
                    return;
                }
                
            }
            //check for IND
            for (int i = 0; i < IND.Length; i++)
            {
                if (inp == IND[i])
                {
                    Result.Text += "(" + inp + "," + IND[i] + "," + lineNo + ")" + Environment.NewLine;
                    return;
                }
                
            }
            
        }

        void ADDSUBToken(string inp, int lineNo) // plusOpt Token
        {
            Result.Text += "(" + inp + ",ADDSUB," + lineNo + ")" + Environment.NewLine;
        }
        void DIVMULMODToken(string inp, int lineNo) // DILMULMOD Token
        {
            Result.Text += "(" + inp + ",DIVMUL," + lineNo + ")" + Environment.NewLine;
        }
        void assignOptToken(string inp, int lineNo) // assignOpt Token
        {
            string[] assignOPT = { "=", "+=", "-=", "/=", "*=","%=" };
            for (int i = 0; i < 6; i++)
            {
                
                if (inp == assignOPT[i])
                {
                    if (assignOPT[i] == "=")
                    {
                        Result.Text += "(" + inp + ",=," + lineNo + ")" + Environment.NewLine;
                    }
                    else
                    {
                        Result.Text += "(" + inp + ",AO," + lineNo + ")" + Environment.NewLine;
                    }
                    return;
                }
            }

        }


        void relationOptToken(string inp, int lineNo) // relationOpt Token
        {
            Result.Text += "(" + inp + ",RO," + lineNo + ")" + Environment.NewLine;
        }
        
        void openRoundBracketToken(string inp, int lineNo) // openRoundBracket Token
        {
            Result.Text += "(" + inp + ",(," + lineNo + ")" + Environment.NewLine;
        }
        void closingRoundBracketToken(string inp, int lineNo)//ClosingRoundBracket Token
        {
            Result.Text += "(" + inp + ",)," + lineNo + ")" + Environment.NewLine;
        }
        void openCurlyBracketToken(string inp, int lineNo) // openCurlyBracket Token
        {
            Result.Text += "(" + inp + ",{," + lineNo + ")" + Environment.NewLine;
        }
        void closingCurlyBracketToken(string inp, int lineNo)//ClosingCurlyBracket Token
        {
            Result.Text += "(" + inp + ",}," + lineNo + ")" + Environment.NewLine;
        }

        void openSquareBracketToken(string inp, int lineNo) // openSquareBracket Token
        {
            Result.Text += "(" + inp + ",[," + lineNo + ")" + Environment.NewLine;
        }
        void closingSquareBracketToken(string inp, int lineNo)//ClosingSquareBracket Token
        {
            Result.Text += "(" + inp + ",]," + lineNo + ")" + Environment.NewLine;
        }

        void colonToken(string inp, int lineNo) // Colon token
        {
            Result.Text += "(" + inp + ",:," + lineNo + ")" + Environment.NewLine;
        }

       
        void doubleColonToken(string inp, int lineNo) // DoubleColon token
        {
            Result.Text += "(" + inp + ",::," + lineNo + ")" + Environment.NewLine;
        }
        void terminatorToken(string inp, int lineNo) // ; token
        {
            Result.Text += "(" + inp + ",;," + lineNo + ")" + Environment.NewLine;
        }
        void invalid2Token(string inp ,int lineNo) // invalid token
        {
            areAllTokens = false;
            Result.Text += "invalid Token \"" +inp+ "\" @ line no: " + lineNo + Environment.NewLine;
        }
        void invalidToken(int lineNo) // invalid token
        {
            areAllTokens = false;
            Result.Text += "invalid Token \"" + temp + "\" @ line no: " + lineNo + Environment.NewLine;
        }


        // DFA FUNCTIONS*******************************************************************

        // IDENTIFIER *****

        int transIdentifier(int cur, char inp)
        {
            int[,] indTrans = { { 1, 2, 2 }, { 1, 1, 2 }, { 2, 2, 2 } };
            if (inp == '_' || (inp >= 'a' && inp <= 'z') || (inp >= 'A' && inp <= 'Z'))
            {
                return indTrans[cur, 0];
            }
            else if (inp >= '0' && inp <= '9')
            {
                return indTrans[cur, 1];
            }
            else
            {
                return indTrans[cur, 2];
            }
        }

        bool DfaIdentifier(string inp)
        {
            int cur = 0;
            int final = 1;
            for (int i = 0; i < inp.Length; i++)
            {
                cur = transIdentifier(cur, inp[i]);
            }

            if (cur == final)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //STRING ****
        int transString(int cur, char inp)
        {
            int[,] stringTrans = { { 1, 4, 4 }, { 2, 1, 3 }, { 4, 4, 4 }, { 1, 1, 1 }, { 4, 4, 4 } };

            if (inp == '"')
            {
                return stringTrans[cur, 0];
            }
            else if (inp != '"' && inp != '\\')
            {
                return stringTrans[cur, 1];
            }
            else if (inp == '\\')
            {
                return stringTrans[cur, 2];
            }
            else
            {
                return 4;
            }
        }

        bool DfaString(string inp)
        {
            int cur = 0;
            int final = 2;
            for (int i = 0; i < inp.Length; i++)
            {
                cur = transString(cur, inp[i]);
            }

            if (cur == final)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        // CHAR *********
        int transChar(int cur, char inp)
        {
            int[,] charTrans = { { 1, 6, 6 }, { 5, 2, 3 }, { 5, 6, 6 }, { 4, 4, 4 }, { 5, 6, 6 }, { 6, 6, 6 }, { 6, 6, 6 } };
            if (inp == '\'')
            {
                return charTrans[cur, 0];
            }
            if ((inp >= 'a' && inp <= 'z') || (inp >= 'A' && inp <= 'Z') || (inp >= '0' && inp <= '9') || (inp == ' ') || inp == '$' || inp == '£' || inp == '%' || inp == '^' || inp == '&' || inp == '*' || inp == '(' || inp == ')' || inp == '!' || inp == '@' || inp == '#' || inp == '~' || inp == '/' || inp == '?' || inp == '<' || inp == '>' || inp == ',' || inp == '.' || inp == '"')
            {
                return charTrans[cur, 1];
            }
            if (inp == '\\')
            {
                return charTrans[cur, 2];
            }
            else
            {
                return 6;
            }
        }

        bool DfaChar(string inp)
        {
            int cur = 0;
            int final = 5;
            for (int i = 0; i < inp.Length; i++)
            {
                if (cur == 3)
                {
                    if (inp[i] == 'n' || inp[i] == 't' || inp[i] == 'r' || inp[i] == '\\' || inp[i] == '\'' || inp[i] == '\"')
                    {
                        cur = transChar(cur, inp[i]);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                    cur = transChar(cur, inp[i]);
            }

            if (cur == final)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //INT ********************
        int transInt(int cur, char inp)
        {
            int[,] intTrans = { { 1, 2, 2 }, { 1, 3, 3 }, { 1, 3, 3 }, { 3, 3, 3 } };

            if (inp >= '0' && inp <= '9')
            {
                return intTrans[cur, 0];
            }
            if (inp == '+')
            {
                return intTrans[cur, 1];
            }
            if (inp == '-')
            {
                return intTrans[cur, 2];
            }
            else
            {
                return 3;
            }

        }

        bool DfaInt(string inp)
        {
            int cur = 0;
            int final = 1;
            for (int i = 0; i < inp.Length; i++)
            {
                cur = transInt(cur, inp[i]);
            }

            if (cur == final)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        // FLOAT ******************
        int transFloat(int cur, char inp)
        {

            int[,] TT = { { 0, 1 }, { 2, 3 }, { 2, 3 }, { 3, 3 } };

            if (inp >= '0' && inp <= '9')
            {
                return TT[cur, 0];
            }
            if (inp == '.')
            {
                return TT[cur, 1];
            }
            else
            {
                return 3;
            }
        }
        bool DfaFloat(string inp)
        {

            int cur = 0;
            int final = 2;
            for (int i = 0; i < inp.Length; i++)
            {
                cur = transFloat(cur, inp[i]);
            }

            if (cur == final)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        // Keyword ******************
        bool keyword_function(string input)
        {
            string[] keyword = { "bool","INC", "DEC", "true", "false", "int", "float", "string", "char", "private", "public", "protected", "var", "till", "from", "func", "class", "this", "break", "loop", "try", "catch", "AND", "OR", "NOT", "childOf", "static", "namespace", "return", "$include", "std", "using", "array", "cout", "cin", "shift", "in", "if", "else", "new", "unit", "default", "iostream", "Enter" };
            for (int i = 0; i < keyword.Length; i++)
            {
                if (input == keyword[i])
                {
                    return true;
                }
                
            }
            return false;
        }
        
        bool IsOpt(char j)
        {
            char[] opt = { ';', '(', ')', '{', '}', ':', '-', '/', '*', '=', '>', '<', '!', '%', '+','-' };
            for (int i = 0; i < 16; i++)
            {
                if (j == opt[i])
                {
                    return true;
                }
            }
            return false; 
        }

        // DFA FUNCTIONS

        bool IsSpiliter(char inp)
        {
            char[] spt = { ' ', '#', ',', '"', '\\', '\'', ';', ':', '(', ')', '[', ']', '{', '}', '+', '=', '-', '<', '>', '.', '/', '%', '!', '*', '\n' };
            for (int i = 0; i < 25; i++)
            {
                if (inp == spt[i])
                {
                    return true;
                }
            }
            return false;
        }
        
        private void btnAnalyze_Click(object sender, EventArgs e)
        {
            Result.Text = "";
            temp = "";
            inp = "";
            lineNo = 1;
            initialize();
            Analyze();
            if (areAllTokens)
            {
                txtConsole.Text = "Tokens Successfully Generated!";
            }
            else txtConsole.Text = "Invaild Tokens Found";
            txtConsole.Text += Environment.NewLine;
            
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clear();
        }
        
        //****** CLEAR *******
        void clear()
        {
            Result.Text = "";
            Code.Text = "";
            temp = "";
            inp = "";
            lineNo = 1;

        }

        void initialize()
        {
            str = false; stChar = false; ROtoken = false; DivMulMod = false; sign = false; invalid = false;
            terminator = false; doubleColon = false; colon = false; opRdBrak = false; clRdBrak = false;
            opSqBrak = false; clSqBrak = false; opCrBrak = false; clCrBrak = false; addSub = false; invalid2 = false;
            isNewLine = false; ext = false; ins = false; onlyDot = false; AOtoken = false; isComma = false; singleComm = false; multiComm = false;
            strADDSUB = ""; strRO = ""; strDIV = ""; strInv = ""; strInv2 = ""; strSign = "";
            areAllTokens = true;
            i = 0;
        }
        //***** ANALYZE ********
 

        void Analyze()
        {
            inp = Code.Text;
            for (i = 0; i < inp.Length; i++)
            {
                if (inp[i] != ' ' && inp[i] != '@' && inp[i] != '#' && inp[i] != ',' && inp[i] != '"' && inp[i] != '\\' && inp[i] != '\'' && inp[i] != ';' && inp[i] != ':' && inp[i] != '(' && inp[i] != ')' && inp[i] != '[' && inp[i] != ']' && inp[i] != '{' && inp[i] != '}' && inp[i] != '+' && inp[i] != '=' && inp[i] != '-' && inp[i] != '<' && inp[i] != '>' && inp[i] != '.' && inp[i] != '/' && inp[i] != '%' && inp[i] != '!' && inp[i] != '*' && inp[i] != '\n' && i != inp.Length -1)
                {
                    temp += inp[i];
                    //txtTesting.Text += "temp¬¬"+temp + "¬¬inp¬¬" +inp[i]+ Environment.NewLine
                }
                else
                {
                    //Result.Text += Environment.NewLine + "skiped¬¬" + inp[i]; 
                    temp = temp.TrimEnd(' ');

                    if (inp[i] == '\n')
                    {
                        if (str)
                        {
                            str = false;
                        }
                        if (stChar)
                        {
                            stChar = false;
                        }

                        //lineNo++;
                    }                    

                    if (inp[i] == '"' && !stChar)
                    {
                        if (str && !stChar)
                        {
                           // temp += '"';
                            str = false;
                        }
                        else
                        {
                            if (temp != "")
                            {
                                callDfa();
                                temp = "";
                            }
                            str = true;
                        }
                    }
                    if (inp[i] == '\'' && !str)
                    {
                        if (stChar)
                        {
                         //   temp += '\'';
                            stChar = false;
                        }
                        else
                        {
                            if (temp != "")
                            {
                                callDfa();
                                temp = "";
                            }
                            stChar = true;
                        }
                    }
                    if (inp[i] == ' ')
                    {
                        //continue;
                    }
                    if (!str && !stChar)
                    {
                        if (inp[i] == '+') // + opt
                        {
                            if (i < inp.Length-1 && inp[i+1] == '=')
                            {
                                ROtoken = true;
                                strRO = "+=";
                                i++;
                            }
                            else
                            {
                                if ((string.IsNullOrWhiteSpace(temp) || !temp.All(char.IsDigit)) && (i < inp.Length -1 && char.IsDigit(inp[i+1])))
                                {
                                    
                                    strSign = "";
                                    sign = true;
                                }
                                else
                                {
                                    addSub = true;
                                    strADDSUB = "+";
                                }  
                                //if ((temp.All(char.IsDigit) && !string.IsNullOrWhiteSpace(temp)) || ((string.IsNullOrWhiteSpace(temp) || !temp.All(char.IsDigit)) && IsSpiliter(inp[i+1]))) /*|| (temp is null/empty OR temp is no digit) && inp[i+1] != splitter */
                                //{
                                //    addSub = true;
                                //    strADDSUB = "+";
                                //}
                                //else
                                //{
                                //    strSign = "+";
                                //    sign = true;
                                //}
                            }
                        }
                        else if (inp[i] == '-') // - opt
                        {
                            if (i < inp.Length - 1 && inp[i + 1] == '=')
                            {
                                ROtoken = true;
                                strRO = "-=";
                                i++;
                            }
                            else
                            {
                                if ((string.IsNullOrWhiteSpace(temp) || !temp.All(char.IsDigit)) && (i < inp.Length - 1 && char.IsDigit(inp[i + 1])))
                                {

                                    strSign = "-";
                                    sign = true;
                                }
                                else
                                {
                                    addSub = true;
                                    strADDSUB = "-";
                                }
                            }
                        }

                        else if (inp[i] == '/') //div
                        {
                            if (i < inp.Length - 1 && inp[i + 1] == '=' )
                            {
                                txtTesting.Text = "chek";
                                ROtoken = true;
                                strRO = "/=";
                                i++;
                            }
                            else if (i < inp.Length-1 && inp[i+1] == '#')
                            {
                                multiComm = true;
                                i+=2;
                            }
                            else
                            {
                                DivMulMod = true;
                                strDIV = "/";
                            }
                        }

                        else if (inp[i] == '*') // mul
                        {
                            if (i < inp.Length - 1 && inp[i + 1] == '=')
                            {
                                ROtoken = true;
                                strRO = "*=";
                                i++;
                            }
                            else
                            {
                                DivMulMod = true;
                                strDIV = "*";
                            }
                        }

                        else if (inp[i] == '%') // mod
                        {
                            if (i < inp.Length - 1 && inp[i + 1] == '=')
                            {
                                ROtoken = true;
                                strRO = "%=";
                                i++;
                            }
                            else
                            {
                                DivMulMod = true;
                                strDIV = "%";
                            }
                        }

                        else if (inp[i] == '.')
                        {
                            if (i < inp.Length - 1 && !char.IsDigit(inp[i+1]))
                            {
                                invalid2 = true;
                                strInv2 = ".";
                            }
                            else if (temp.All(char.IsDigit) || string.IsNullOrWhiteSpace(temp))
                            {
                                temp += ".";
                                continue;
                            }
                            else
                            {
                                onlyDot = true;
                            }
                        }
                        else if (inp[i] == ';')
                        {
                            terminator = true;
                        }
                        else if (inp[i] == ',')
                        {
                            isComma = true;
                        }
                        else if (inp[i] == ':')
                        {
                            if (i < inp.Length - 1 && inp[i+1] == ':')
                            {
                                doubleColon = true;
                                i++;
                            }
                            else
                            {
                                colon = true;
                            }
                        }
                        else if (inp[i] == '(')
                        {
                            opRdBrak = true;
                        }
                        else if (inp[i] == ')')
                        {
                            clRdBrak = true;
                        }
                        else if (inp[i] == '[')
                        {
                            opSqBrak = true;
                        }
                        else if (inp[i] == ']')
                        {
                            clSqBrak = true;
                        }
                        else if (inp[i] == '{')
                        {
                            opCrBrak = true;
                        }
                        else if (inp[i] == '}')
                        {
                            clCrBrak = true;
                        }
                        else if (inp[i] == '<')
                        {
                            if (i < inp.Length - 1 && inp[i+1] == '=')
                            {
                                ROtoken = true;
                                strRO = "<=";
                                i++;
                            }
                            else if (i < inp.Length - 1 && inp[i+1] == '<')
                            {
                                ins = true;
                                i++;
                            }
                            else
                            {
                                ROtoken = true;
                                strRO = "<";
                            }
                            
                        }
                        else if (inp[i] == '>')
                        {
                            if (i < inp.Length - 1 && inp[i + 1] == '=')
                            {
                                ROtoken = true;
                                strRO = ">=";
                                i++;
                            }
                            else if (i < inp.Length - 1 &&  inp[i+1] == '>')
                            {
                                ext = true;
                                i++;
                            }
                            else
                            {
                                ROtoken = true;
                                strRO = ">";
                            }
                        }
                        else if (inp[i] == '!')
                        {
                            if (i < inp.Length - 1 && inp[i+1] == '=')
                            {
                                ROtoken = true;
                                strRO = "!=";
                                i++;
                            }
                            else
                            {
                                invalid2 = true;
                                strInv2 = "!";
                            }
                        }
                        else if (inp[i] == '#')
                        {
                            if (inp[i + 1] == '#')
                            {
                                singleComm = true;
                                i++;
                            }
                            else
                            {
                                invalid = true;
                            }
                        }

                        else if (inp[i] == '=')
                        {
                            if (i < inp.Length - 1 && inp[i + 1] == '=')
                            {
                                ROtoken = true;
                                strRO = "==";
                                i++;
                            }
                            else
                            {
                                 AOtoken = true;
                            }
                        }
                        else if (inp[i] == '\n')
                        {
                            isNewLine = true;
                            //lineNo++;
                        }
                        else if (inp[i] == ' ')
                        {
                            txtTesting.Text = "empty";   
                        }
                        else
                        {
                            //txtTesting.Text += i + ")" + inp[i].ToString();
                            temp += inp[i];
                        }

                        throwTokens();

                        temp = "";
                        //if (singleComm)
                        //{
                        //    while (i < inp.Length - 1 && inp[i] != '\n')
                        //    {
                        //        i++;
                        //    }
                        //    singleComm = false;
                        //    //lineNo++;
                        //    //continue;
                        //}
                        if (sign)
                        {
                            temp += strSign;
                            sign = false;
                        }
                        if (onlyDot)
                        {
                            temp += ".";
                            onlyDot = false;
                        }




                    } //if(!str || !stchar) --> end
                    
                    else
                    {

                        txtTesting.Text += temp;
                        if (inp[i]  == '\\' && inp[i+1] == '"')
                        {
                            temp += "\\\"";
                            i += 2;
                            if (inp[i] == '"')
                            {
                                i--;
                                continue;
                            }
                        }
                            temp += inp[i];
                            continue;
                    
                    }
                    
                }
            }
        }

        void throwTokens()
        {
            callDfa();
            if (singleComm)
            {
                while (i < inp.Length - 1 && inp[i] != '\n')
                {
                    i++;
                }
                lineNo++;
                singleComm = false;

            }
            if (multiComm)
            {
                while(i < inp.Length -1)
                {
                    if (inp[i] == '#')
                    {
                        i++;
                        if (inp[i] == '/')
                        {
                            break;
                        }
                    }
                    if (inp[i] == '\n')
                    {
                        lineNo++;
                    }
                    i++;
                }
                i++;
                multiComm = false;
            }
            if (ROtoken)
            {
                txtTesting.Text = "generate ro token" + strRO;
                relationOptToken(strRO, lineNo);
                ROtoken = false;
                strRO = "";
            }
            else if (AOtoken)
            {
                assignOptToken("=", lineNo);
                AOtoken = false;
            }
            if (isComma)
            {
                commaToken(",", lineNo);
                isComma = false;
            }
            if (addSub)
            {
                ADDSUBToken(strADDSUB, lineNo);
                addSub = false;
                strADDSUB = "";
            }
            if (terminator)
            {
                terminatorToken(";", lineNo);
                terminator = false;
            }
            if (DivMulMod)
            {
                DIVMULMODToken(strDIV, lineNo);
                strDIV = "";
                DivMulMod = false;
            }
            if (doubleColon)
            {
                doubleColonToken("::", lineNo);
                doubleColon = false;
            }
            if (colon)
            {
                colonToken(":", lineNo);
                colon = false;
            }
            if (opRdBrak)
            {
                openRoundBracketToken("(",lineNo);
                opRdBrak = false;
            }
            if (clRdBrak)
            {
                closingRoundBracketToken(")", lineNo);
                clRdBrak = false; 
            }
            if (opCrBrak)
            {
                openCurlyBracketToken("{",lineNo);
                opCrBrak = false;
            }
            if (clCrBrak)
            {
                closingCurlyBracketToken("}", lineNo);
                clCrBrak = false;
            }
            if (opSqBrak)
            {
                openSquareBracketToken("[", lineNo);
                opSqBrak = false;
            }
            if (clSqBrak)
            {
                closingSquareBracketToken("]", lineNo);
                clSqBrak = false;
            }
            
            if (ext)
            {
                extractionToken(">>", lineNo);
                ext = false;
            }
            if (ins)
            {
                insertionToken("<<",lineNo);
                ins = false;
            }
            
            if (invalid2)
            {
                invalid2Token(strInv2,lineNo);
                strInv = "";
                invalid2 = false;
            }
            
        }


        void callDfa()
        {

            if (isNewLine)
            {
                lineNo++;
                isNewLine = false;
            }
            if (temp == "")
            {

            }
            else if (keyword_function(temp))
            {
                KeyWordToken(temp, lineNo);
            }
            else if (DfaInt(temp))
            {
                intToken(temp, lineNo);
            }
            else if (DfaChar(temp))
            {
                charToken(temp, lineNo);
            }
            else if (DfaFloat(temp))
            {
                floatToken(temp, lineNo);
            }
            else if (DfaString(temp))
            {
                stringToken(temp, lineNo);
            }
            else if (DfaIdentifier(temp))
            {
                IdentifierToken(temp, lineNo);
            }
            else
            {
                invalid = true;
                strInv = temp;
            }
            if (invalid)
            {
                invalidToken(lineNo);
                invalid = false;
            }

        }
        private void DoubleClick_clear(object sender, EventArgs e)
        {
            txtTesting.Text = "";
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            c.enterFunc = false;
            temp = "";
            inp = "";
            //txtToken.Text = Result.Text;
            string cpy = Result.Text;
            txtToken.Text = cpy;

            string tk = txtToken.Text;
            tk = tk.TrimEnd();

            if (tk != "")
            {
                txtToken.Text = tk + Environment.NewLine + "($,$,0)";
            }
            else
            {
               txtToken.Text = "($,$,0)";

            }
            txtSyntax.Text = "";
            txtSyntax.Text += " CP" + "\t\t" + "VP" + "\t\t" + "LineNo" + Environment.NewLine;
            txtSyntax.Text += "----------------------------------------------------------------------------" + Environment.NewLine;
            int arryIndex = 0;
            inp = txtToken.Text;
            for (int i = 0; i < inp.Length; i++)
            {

                while (i < inp.Length && inp[i] != '\n')
                {
                    temp += inp[i];
                    i++;
                }
                temp = temp.TrimEnd('\n');

                if (i < inp.Length && inp[i] == '\n')
                {
                    temp = temp.Substring(1, temp.Length - 3);
                }
                else
                {
                    temp = temp.Substring(1, temp.Length - 2);

                }

                int c1 = temp.IndexOf(',');
                if (c1 == 0)
                {
                    c1++;
                }
                int c2 = temp.Substring(c1 + 1).IndexOf(',') + (c1 + 1);
                if (temp[c1 + 1] == ',')
                {
                    c2++;
                }
                try
                {
                    c.tokenStream[arryIndex].VP = temp.Substring(0, c1);
                    c.tokenStream[arryIndex].CP = temp.Substring(c1 + 1, c2 - (c1 + 1));
                    c.tokenStream[arryIndex].lineNo = Convert.ToInt32(temp.Substring(c2 + 1));

                    txtSyntax.Text += c.tokenStream[arryIndex].CP + "\t\t";
                    txtSyntax.Text += c.tokenStream[arryIndex].VP + "\t\t";
                    txtSyntax.Text += c.tokenStream[arryIndex].lineNo.ToString() + Environment.NewLine;
                    arryIndex++;
                    temp = "";

                }
                catch (Exception exp)
                {
                    MessageBox.Show("Invalid Token Found! " +temp+"   " + exp.ToString());
                }
            }//for
            c.ind = 0;
            //txtSyntax.Text += tokenStream[ind].CP;
            txtSyntax.Text += Environment.NewLine;

            if (c.CLASS())
            {
                if (!c.enterFunc)
                {
                    txtConsole.Text += "Enter Function Not Found!" +Environment.NewLine;
                }
                else
                txtConsole.Text += "Syntax is Correct";

            }
            else
            {
                txtConsole.Text += "Invalid Syntax at " + c.tokenStream[c.ind].CP + " at lineNo " + c.tokenStream[c.ind].lineNo;
                //txtSyntax.Text += "Invalid Syntax";
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtToken.Text = "";
            txtSyntax.Text = "";
            inp = "";
            temp = "";
        }

    }
}
