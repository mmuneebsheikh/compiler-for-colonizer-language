﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LexcalGui
{
    public partial class SemanticConsole : Form
    {
        public SemanticConsole()
        {
            InitializeComponent();
        }


        public void RedeclarationError(string ID, int lineNo)
        {
            txtConsole.Text += ID + " is already declared. Redeclaration Error at lineNo: " + lineNo.ToString() + Environment.NewLine;
        }
        public void TypeMismatchError(string t, string rt)
        {
            txtConsole.Text += "Type Mismatch cannot convert type '" + rt + "' to '" + t + "'" + Environment.NewLine;
        }
        public void UndeclaredID(string ID, int lineNo)
        {
            txtConsole.Text += ID + " Identifier Not Declared at lineNo:" + lineNo.ToString();
            txtConsole.Text += Environment.NewLine;
        }


    }
}
