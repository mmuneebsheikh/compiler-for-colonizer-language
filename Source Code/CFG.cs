﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace LexcalGui
{
    class CFG
    {

        SemanticConsole SC = new SemanticConsole();

        public CFG()
        {
            SC.Show();
        }

        public int ind = 0;
        public bool enterFunc = false;
        public struct Tokens
        {
            public string CP;
            public string VP;
            public int lineNo;

        }
        public Tokens[] tokenStream = new Tokens[1000000];


        /// <semantic>
        /// 
        /// </semantic>


        int SCount = -1;
        List<int> scopeStack = new List<int>();


        //
        //
        //DECL symbol Table
        //
        //

        public struct DeclSymTbl
        {
            public string name;
            public string type;
            public int scope;
        }
        string opt, T, N;
        List<DeclSymTbl> DST = new List<DeclSymTbl>();

        public string LookUp(string name, int scope)
        {
            MessageBox.Show("looking: " + name);
            for (int j = 0; j < scopeStack.Count; j++)
            {
                int s = scopeStack[j];
                for (int i = 0; i < DST.Count; i++)
                {
                    if (DST[i].scope == s)
                    {
                        MessageBox.Show("now: " + DST[i].name);
                        if (name == DST[i].name)
                        {
                            return DST[i].type;
                        }
                    }

                }
            }


            return "";

        }




        void InsertDecl(string name, string type, int scope)
        {
            DeclSymTbl decl = new DeclSymTbl();
            decl.name = name;
            decl.type = type;
            decl.scope = scope;

            DST.Add(decl);
            MessageBox.Show("inserted: " + decl.name);
        }




        string Comp(string lT, string rT, string opt)
        {

            if (opt == "=")
            {
                if (lT == rT)
                {
                    return lT;
                }
                else if (lT == "float" && rT == "int")
                {
                    return "flaot";
                }
                else if (lT == "string" && rT == "char")
                {
                    return "string";
                }
                else
                {
                    return "";
                }
            }
            else if (opt == "ADDSUB" || opt == "DIVMUL")
            {

                if (lT == rT)
                {
                    return lT;
                }
                else if (lT == "float" || rT == "float")
                {
                    if (lT == "string" || lT == "char" || lT == "bool" || rT == "string" || rT == "char" || rT == "bool")
                    {
                        return "";
                    }
                    return "float";
                }
                else if (lT == "string" || rT == "string")
                {
                    if (lT == "int" || lT == "float" || lT == "bool" || rT == "int" || rT == "float" || rT == "bool")
                    {
                        return "";
                    }
                    return "string";
                }
                return "";
            }
            else if (opt == "RO")
            {

                //bool a = 'c' < '2';
                //bool a = 'c' < 2;
                //bool a = 2 < '2';
                //bool a = 2 < 2.2;
                //bool a = 2.2 < 2;

                if (lT != "string" && rT != "string")
                {
                    return "bool";
                }
                else return "";
            }
            else if (opt == "LO")
            {
                if (lT == "bool" && rT == "bool")
                {
                    return "bool";
                }
                return "";
            }
            else
            {
                MessageBox.Show("Invalid Operator in Semantic");
                return "";
            }

        }




        //
        //
        //MAIN BODY
        //
        //
        public bool MAIN_BODY()
        {
            if (MAIN_MST())
            {
                return true;
            }
            else { return false; }
        }
        bool MAIN_SST()
        {
            if (CLASS())
            {
                return true;
            }
            else return false;
        }


        bool MAIN_MST()
        {
            if (MAIN_SST())
            {
                if (MAIN_MST())
                {
                    return true;
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "$")
            {
                return true;
            }
            else { return false; }
        }

        //
        //
        //s_st
        //yahan function bhi call hosakte hain class ki statements hain
        //

        bool S_ST()
        {
            if (DECL(SCount))
            {
                return true;
            }
            else if (ARR())
            {
                return true;
            }
            else if (FUNC())
            {
                return true;
            }
            else if (tokenStream[ind].CP == "ID")
            {

                if (tokenStream[ind+1].CP == "=")
                {
                    if (SST_ASSIGN())
                    {
                        return true;
                    }
                    else return false;
                }
                else if (COND())
                {
                    return true;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == ";")
            {
                ind++;
                return true;
            }
            else { return false; }
        }
        bool M_ST()
        {
            if (S_ST())
            {
                if (M_ST())
                {
                    return true;
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "}")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //
        //
        //CLASS
        //
        //
        bool CLASS_X1()
        {
            if (tokenStream[ind].CP == ",")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    if (CLASS_X1())
                    {
                        return true;
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "{")
            {
                return true;
            }
            else { return false; }
        }

        bool CLASS_X()
        {
            if (tokenStream[ind].CP == "childOf")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    if (CLASS_X1())
                    {
                        return true;
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "{")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool AM()
        {
            if (tokenStream[ind].CP == "AM")
            {
                ind++;
                return true;
            }
            else if (tokenStream[ind].CP == "class")
            {
                return true;
            }
            else return false;
        }

        //bool CLASS_STATEMENT()
        //{
        //    if (M_ST())
        //    {
        //        return true;
        //    }
        //    else if (tokenStream[ind].CP == "}")
        //    {
        //        return true;
        //    }
        //    else { return false; }
        //}
            
        bool CLASS_BODY()
        {
            if (tokenStream[ind].CP == "{")
            {
                SCount++;
                scopeStack.Add(SCount);
                ind++;
                if (M_ST())
                {
                    if (tokenStream[ind].CP == "}")
                    {
                        scopeStack.RemoveAt(scopeStack.Count - 1);
                        ind++;
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        }


        public bool CLASS()
        {
            if (AM())
            {
                if (tokenStream[ind].CP == "class")
                {
                    ind++;
                    if (tokenStream[ind].CP == "ID")
                    {
                        ind++;
                        if (CLASS_X())
                        {
                            if (CLASS_BODY())
                            {
                                if (CLASS())
                                {
                                    return true;
                                }
                                else return false;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "$")
            {
                ind++;
                return true;
            }
            else return false;
        }


        //
        //
        //DECL
        //
        //
        bool A(string t, int S)
        {
            string rt;

            if (tokenStream[ind].CP == "string" || tokenStream[ind].CP == "char")
            {
                rt = tokenStream[ind].CP;
                T = Comp(t, rt, opt);
                if (T == "")
                {
                    SC.TypeMismatchError(t, rt);
                }
                ind++;
                return true;
            }
            else if (tokenStream[ind].CP == "new")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    if (tokenStream[ind].CP == "(")
                    {
                        ind++;
                        if (C_PARAM())
                        {
                            if (tokenStream[ind].CP == ")")
                            {
                                ind++;
                                return true;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (COND())
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        bool ASSIGN(string t, int S)
        {
            if (tokenStream[ind].CP == "=")
            {
                opt = tokenStream[ind].VP;
                ind++;
                if (A(t, S))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (tokenStream[ind].CP == ";")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool D1_1(string t, int S) // multi var declaration me
        {
            if (tokenStream[ind].CP == "ID")
            {
                N = tokenStream[ind].VP;
                if (LookUp(N, S) == "")
                {
                    InsertDecl(N, t, S);
                }
                else
                {
                    SC.RedeclarationError(N,tokenStream[ind].lineNo);
                }

                ind++;
                if (ASSIGN(t,S))
                {
                    if (tokenStream[ind].CP == ";")
                    {
                        ind++;
                        if (D1_1(t,S))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else if (tokenStream[ind].CP == "}")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool D1(int S)
        {
            if (tokenStream[ind].CP == "ID")
            {
                N = tokenStream[ind].VP;
                ind++;
                if (tokenStream[ind].CP == ":")
                {
                    ind++;
                    if (tokenStream[ind].CP == "DT" )
                    {
                        T = tokenStream[ind].VP;
                        if (LookUp(N, S) == "")
                        {
                            InsertDecl(N, T, S);
                        }
                        else
                        {
                            SC.RedeclarationError(N, tokenStream[ind].lineNo);
                        }
                        ind++;
                        if (ASSIGN(T,S))
                        {
                            return true;
                        }
                        else { return false; }
                    }
                    else if (tokenStream[ind].CP == "ID")
                    {
                        ind++;
                        if (ASSIGN(T, S))
                        {
                            return true;
                        }
                        else { return false; }
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "DT" || tokenStream[ind].CP == "ID")
            {
                if (tokenStream[ind].CP == "DT")
                {
                    T = tokenStream[ind].VP;
                }
                ind++;
                if (tokenStream[ind].CP == "{")
                {
                    SCount++;
                    scopeStack.Add(SCount);
                    ind++;
                    if (D1_1(T,SCount))
                    {
                        if (tokenStream[ind].CP == "}")
                        {
                            scopeStack.RemoveAt(scopeStack.Count - 1);
                            ind++;
                            return true;
                        }
                        else { return false; }
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else
            {
                return false;
            }
        }

        public bool DECL(int S)
        {
            if (tokenStream[ind].CP == "var")
            {
                ind++;
                if (D1(S))
                {
                    if (tokenStream[ind].CP == ";")
                    {
                        ind++;
                        return true;
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else { return false; }
        }

        //
        //
        //ARRAY
        //
        //

        bool ARR_I1()
        {
            if (tokenStream[ind].CP == ",")
            {
                ind++;
                if (tokenStream[ind].CP == "int" || tokenStream[ind].CP == "string" || tokenStream[ind].CP == "char" || tokenStream[ind].CP == "float")
                {
                    ind++;
                    if (ARR_I1())
                    {
                        return true;
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "}")
            {
                return true;
            }
            else { return false; }
        }
        bool ARR_I()
        {
            if (tokenStream[ind].CP == "int" || tokenStream[ind].CP == "string" || tokenStream[ind].CP == "char" || tokenStream[ind].CP == "float")
            {
                ind++;
                if (ARR_I1())
                {
                    return true;
                }
                else { return false; }
            }
            else { return false; }
        }
        bool ARR_A()
        {
            if (ARR_I())
            {
                return true;
            }
            else if (ARR_J())
            {
                return true;
            }
            else { return false; }
        }
        bool ARR_J1()
        {
            if (tokenStream[ind].CP == ",")
            {
                ind++;
                if (tokenStream[ind].CP == "{")
                {
                    ind++;
                    if (ARR_A())
                    {
                        if (tokenStream[ind].CP == "}")
                        {
                            ind++;
                            if (ARR_J1())
                            {
                                return true;
                            }
                            else { return false; }
                        }
                        else { return false; }
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "}")
            {
                return true;
            }
            else { return false; }
        }
        bool ARR_J()
        {
            if (tokenStream[ind].CP == "{")
            {
                ind++;
                if (ARR_A())
                {
                    if (tokenStream[ind].CP == "}")
                    {
                        ind++;
                        if (ARR_J1())
                        {
                            return true;
                        }
                        else { return false; }
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else { return false; }
        }
        bool ARR_ASSIGN()
        {
            if (tokenStream[ind].CP == "=")
            {
                ind++;
                if (tokenStream[ind].CP == "{")
                {
                    ind++;
                    if (ARR_A())
                    {
                        if (tokenStream[ind].CP == "}")
                        {
                            ind++;
                            return true;
                        }
                        else { return false; }
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == ";")
            {
                return true;
            }
            else { return false; }
        }
        bool ARR_SIZE()
        {
            //if (tokenStream[ind].CP == "int" || tokenStream[ind].CP == "string" || tokenStream[ind].CP == "char" || tokenStream[ind].CP == "float")
            //{
            //    ind++;
            //    return true;
            //}
            //else if (tokenStream[ind].CP == "]")
            //{
            //    return true;
            //}
            if (EXP())
            {
                return true;
            }
            else if (tokenStream[ind].CP == "]")
            {
                return true;
            }
            else { return false; }
        }
        bool ARR_DIM()
        {
            if (tokenStream[ind].CP == "[")
            {
                ind++;
                if (ARR_SIZE())
                {
                    if (tokenStream[ind].CP == "]")
                    {
                        ind++;
                        if (ARR_DIM())
                        {
                            return true;
                        }
                        else { return false; }
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "=" || tokenStream[ind].CP == ";")
            {
                return true;
            }
            else { return false; }
        }
        bool ARR()
        {
            if (tokenStream[ind].CP == "array")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    if (tokenStream[ind].CP == ":")
                    {
                        ind++;
                        if (tokenStream[ind].CP == "DT")
                        {
                            ind++;
                            if (tokenStream[ind].CP == "[")
                            {
                                ind++;
                                if (ARR_SIZE())
                                {
                                    if (tokenStream[ind].CP == "]")
                                    {
                                        ind++;
                                        if (ARR_DIM())
                                        {
                                            if (ARR_ASSIGN())
                                            {
                                                if (tokenStream[ind].CP == ";")
                                                {
                                                    ind++;
                                                    return true;
                                                }
                                                else { return false; }
                                            }
                                            else { return false; }
                                        }
                                        else { return false; }
                                    }
                                    else { return false; }
                                }
                                else { return false; }
                            }
                            else { return false; }
                        }
                        else { return false; }
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else { return false; }
        }


        //
        //
        //IF_ELSE
        //
        //
        bool ifElse_body()
        {
            if (tokenStream[ind].CP == "{")
            {
                ind++;
                if (S_MST())
                {
                    if (tokenStream[ind].CP == "}")
                    {
                        ind++;
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (S_SST())
            {
                return true;
            }
            else return false;
        }

        bool ELSE()
        {
            if (tokenStream[ind].CP == "else")
            {
                ind++;
                if (ifElse_body())
                {
                    return true;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "$")
            {
                return true;
            }
            else return false;
        }

        public bool IF_ELSE()
        {
            if (tokenStream[ind].CP == "if")
            {
                ind++;
                if (tokenStream[ind].CP == "(")
                {
                    ind++;
                    if (COND())
                    {
                        if (tokenStream[ind].CP == ")")
                        {
                            ind++;
                            if (ifElse_body())
                            {
                                if (ELSE())
                                {
                                    return true;
                                }
                                else return false;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        }



        //
        //
        //FUNCTION
        //
        //

        bool Func_DIM()
        {
            if (tokenStream[ind].CP == "[")
            {
                ind++;
                if (tokenStream[ind].CP == "]")
                {
                    ind++;
                    if (Func_DIM())
                    {
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "," || tokenStream[ind].CP == ")")
            {
                return true;
            }
            else return false;
        }
        bool D_P1()
        {
            if (tokenStream[ind].CP == ",")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    if (tokenStream[ind].CP == ":")
                    {
                        ind++;
                        if (tokenStream[ind].CP == "DT")
                        {
                            ind++;
                            if (Func_DIM())
                            {
                                if (D_P1())
                                {
                                    return true;
                                }
                                else return false;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == ")")
            {
                return true;
            }
            else return false;
        }
        bool D_PARAM()
        {
            if (tokenStream[ind].CP == "ID")
            {
                ind++;
                if (tokenStream[ind].CP == ":")
                {
                    ind++;
                    if (tokenStream[ind].CP == "DT")
                    {
                        ind++;
                        if (Func_DIM())
                        {
                            if (D_P1())
                            {
                                return true;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false; 
            }
            else if (tokenStream[ind].CP == ")")
            {
                return true;
            }
            else return false;
        }

        bool ENTER_FUNC()
        {
            if (tokenStream[ind].CP == "ID")
            {
                ind++;
                if (tokenStream[ind].CP == "(")
                {
                    ind++;
                    if (D_PARAM())
                    {
                        if (tokenStream[ind].CP == ")")
                        {
                            ind++;
                            if (tokenStream[ind].CP == ":")
                            {
                                ind++;
                                if (tokenStream[ind].CP == "DT" || tokenStream[ind].CP == "unit")
                                {
                                    ind++;
                                    if (tokenStream[ind].CP == "=")
                                    {
                                        ind++;
                                        if (S_BODY())
                                        {
                                            return true;
                                        }
                                        else return false;
                                    }
                                    else return false;
                                }
                                else return false;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "Enter")
            {
                enterFunc = true;
                ind++;
                if (tokenStream[ind].CP == "(")
                {
                    ind++;
                    if (tokenStream[ind].CP == ")")
                    {
                        ind++;
                        if (tokenStream[ind].CP == ":")
                        {
                            ind++;
                            if (tokenStream[ind].CP == "unit")
                            {
                                ind++;
                                if (tokenStream[ind].CP == "=")
                                {
                                    ind++;
                                    if (S_BODY())
                                    {
                                        return true;
                                    }
                                    else return false;
                                }
                                else return false;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        }
        public bool FUNC()
        {
            if (tokenStream[ind].CP == "func")
            {
                ind++;
                if (ENTER_FUNC())
                {
                    return true;
                }
                else return false;
            }
            else return false;
        }

        //
        //
        //SHIFT_IN
        //
        //
        bool InDef_BODY()
        {
            if (tokenStream[ind].CP == "break")
            {
                ind++;
                if (tokenStream[ind].CP == ";")
                {
                    ind++;
                    return true;
                }
                else return false;
            }
            //else if (S_SST())
            //{
            //    if (InDef_BODY())
            //    {
            //        return true;
            //    }
            //    else return false;
            //}
            else return false;
        }

        bool SHIFT_DEFAULT()
        {
            if (tokenStream[ind].CP == "default")
            {
                ind++;
                if (tokenStream[ind].CP == ":")
                {
                    ind++;
                    if (InDef_BODY())
                    {
                        if (IN())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "}")
            {
                return true;
            }
            else return false;
        }



        bool IN()
        {
            if (tokenStream[ind].CP == "in")
            {
                ind++;
                if (IN_TYPE())
                {
                    if (tokenStream[ind].CP == ":")
                    {
                        ind++;
                        if (InDef_BODY())
                        {
                            if (IN())
                            {
                                return true;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "default" || tokenStream[ind].CP == "}")
            {
                return true;
            }
            else return false;
        }

        bool Shift_BODY()
        {
            if (tokenStream[ind].CP == "in")
            {
                if (IN())
                {
                    if (SHIFT_DEFAULT())
                    {
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "}")
            {
                return true;
            }
            else return false;
        }

        public bool SHIFT_IN()
        {
            if (tokenStream[ind].CP == "shift")
            {
                ind++;
                if (tokenStream[ind].CP == "(")
                {
                    ind++;
                    if (COND())
                    {
                        if (tokenStream[ind].CP == ")")
                        {
                            ind++;
                            if (tokenStream[ind].CP == "{")
                            {
                                ind++;
                                if (Shift_BODY())
                                {
                                    if (tokenStream[ind].CP == "}")
                                    {
                                        ind++;
                                        return true;
                                    }
                                    else return false;
                                }
                                else return false;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        }


        bool IN_F()
        {
            if (CONST())
            {
                return true;
            }
            else if (tokenStream[ind].CP == "(")
            {
                if (IN_TYPE())
                {
                    if (tokenStream[ind].CP == ")")
                    {
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        }
        bool CONST()
        {
            if (tokenStream[ind].CP == "int" || tokenStream[ind].CP == "string" || tokenStream[ind].CP == "float" || tokenStream[ind].CP == "char")
            {
                ind++;
                return true;
            }
            else return false;
        }

        bool IN_T1()
        {
            if (tokenStream[ind].CP == "DIVMUL")
            {
                ind++;
                if (IN_F())
                {
                    if (IN_T1())
                    {
                        return true;
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "ADDSUB" || tokenStream[ind].CP == ":")
            {
                return true;
            }
            else return false;
        }

        bool IN_T()
        {
            if (IN_F())
            {
                if (IN_T1())
                {
                    return true;
                }
                else return false;
            }
            else return false;
        }

        bool IN_E1()
        {
            if (tokenStream[ind].CP == "ADDSUB")
            {
                ind++;
                if (IN_T())
                {
                    if (IN_E1())
                    {
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == ":")
            {
                return true;
            }
            else return false;
        }
        bool IN_TYPE()
        {
            if (IN_T())
            {
                if (IN_E1())
                {
                    return true;
                }
                else return false;
            }
            else return false;
        }






        //
        //
        //LOOP
        //
        //

        bool LOOP_X1()
        {
            if (tokenStream[ind].CP == "AO")
            {
                ind++;
                if (EXP())
                {
                    return true;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "INCDEC")
            {
                ind++;
                return true;
            }
            else return false;
        }

        bool LOOP_X()
        {
            if (tokenStream[ind].CP == "ID")
            {
                ind++;
                if (LOOP_X1())
                {
                    return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "INCDEC")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    return true;
                }
                else return false;
            }
            else return false;
        }

        bool LOOP_DC()
        {
            if (tokenStream[ind].CP == ":")
            {
                ind++;
                if (tokenStream[ind].CP == "DT")
                {
                    ind++;
                    if (tokenStream[ind].CP == "=")
                    {
                        ind++;
                        if (EXP())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "=")
            {
                ind++;
                if (EXP())
                {
                    return true;
                }
                else return false;
            }
            else return false;
        }
        bool LOOP1()
        {
            if (tokenStream[ind].CP == "from")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    if (LOOP_DC())
                    {
                        if (tokenStream[ind].CP == ":")
                        {
                            ind++;
                            if (tokenStream[ind].CP == "till")
                            {
                                ind++;
                                if (COND())
                                {
                                    if (tokenStream[ind].CP == ":")
                                    {
                                        ind++;
                                        if (LOOP_X())
                                        {
                                            return true;
                                        }
                                        else return false;
                                    }
                                    else return false;
                                }
                                else return false;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "till")
            {
                ind++;
                if (COND())
                {
                    return true;
                }
                else return false;
            }
            else return false;
        }

        public bool LOOP()
        {
            if (tokenStream[ind].CP == "loop")
            {
                ind++;
                if (tokenStream[ind].CP == "(")
                {
                    ind++;
                    if (LOOP1())
                    {
                        if (tokenStream[ind].CP == ")")
                        {
                            ind++;
                            if (S_BODY())
                            {
                                return true;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        }




        //
        //
        //S_BODY
        //
        //
        bool S_BODY()
        {
            //NOT func decl not Class decl

            if (tokenStream[ind].CP == "{")
            {
                SCount++;
                scopeStack.Add(SCount);
                ind++;
                if (S_MST())
                {
                    if (tokenStream[ind].CP == "}")
                    {
                        scopeStack.RemoveAt(scopeStack.Count-1);
                        ind++;
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        }
        bool S_MST()
        {
            if (S_SST())
            {
                if (S_MST())
                {
                    return true;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "}")
            {
                return true;
            }
            else return false;
        }

        bool S_SST()
        {
            if (tokenStream[ind].CP == ";")
            {
                ind++;
                return true;
            }
            else if (LOOP())
            {
                return true;
            }
            else if (ARR())
            {
                return true;
            }
            else if (DECL(SCount))
            {
                return true;
            }
            else if (SHIFT_IN())
            {
                return true;
            }
            else if (IF_ELSE())
            {
                return true;
            }
            else if (tokenStream[ind].CP == "break")
            {
                ind++;
                if (tokenStream[ind].CP == ";")
                {
                    ind++;
                    return true;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "continue")
            {
                ind++;
                if (tokenStream[ind].CP == ";")
                {
                    ind++;
                    return true;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "return")
            {
                ind++;
                if (tokenStream[ind].CP == "bool" || tokenStream[ind].CP == "string" || tokenStream[ind].CP == "char")
                {
                    ind++;
                    if (tokenStream[ind].CP == ";")
                    {
                        ind++;
                        return true;
                    }
                    else return false;
                }
                else if (COND())
                {
                    if (tokenStream[ind].CP == ";")
                    {
                        ind++;
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (EXP())
            {
                if (tokenStream[ind].CP == ";")
                {
                    ind++;
                    return true;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "INCDEC")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    if (ST_DIM())
                    {
                        if (tokenStream[ind].CP == ";")
                        {
                            ind++;
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (SST_ASSIGN())
            {
                return true;
            }
            else return false;
        }
        bool SST_ASSIGN()
        {
            if (tokenStream[ind].CP == "ID")
            {
                ind++;
                if (ST_DIM())
                {
                    if (SST_A1())
                    {
                        if (tokenStream[ind].CP == ";")
                        {
                            ind++;
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        }
        bool SST_A1()
        {
            if (tokenStream[ind].CP == "INCDEC")
            {
                return true;
            }
            else if (AO())
            {
                if (COND())
                {
                    return true;
                }
                else return false;
            }
            else return false;
        }
        bool AO()
        {
            if (tokenStream[ind].CP == "AO" || tokenStream[ind].CP == "=")
            {
                ind++;
                return true;
            }
            else return false;
        }
        bool ST_DIM()
        {
            if (tokenStream[ind].CP == "[")
            {
                ind++;
                if (INDX())
                {
                    if (tokenStream[ind].CP == "]")
                    {
                        ind++;
                        if (ST_DIM())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "INCDEC" || tokenStream[ind].CP == "AO" || tokenStream[ind].CP == "=" || tokenStream[ind].CP == ";")
            {
                return true;
            }
            else return false;
        }






        //
        //
        //
        //
        //

        //
        //
        //COND
        //
        //


        bool DIM()
        {
            if (tokenStream[ind].CP == "[")
            {
                if (INDX())
                {
                    if (tokenStream[ind].CP == "]")
                    {
                        ind++;
                        if (DIM())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == ";" ||  tokenStream[ind].CP == "RO" || tokenStream[ind].CP == "LO" || tokenStream[ind].CP == "$" || tokenStream[ind].CP == "DIVMUL" || tokenStream[ind].CP == "ADDSUB")
            {
                return true;
            }
            else return false;
        }
        bool COND_ID1()
        {
            if (tokenStream[ind].CP == "(")
            {
                ind++;
                if (C_PARAM())
                {
                    if (tokenStream[ind].CP == ")")
                    {
                        ind++;
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "[")
            {
                if (INDX())
                {
                    if (tokenStream[ind].CP == "]")
                    {
                        ind++;
                        if (DIM())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "RO" || tokenStream[ind].CP == "LO")
            {
                return true;
            }
            else return false;
        }
        bool COND_ID()
        {
            if (tokenStream[ind].CP == "bool")
            {
                ind++;
                return true;
            }
            else if (tokenStream[ind].CP == "NOT")
            {
                ind++;
                if (tokenStream[ind].CP == "bool")
                {
                    ind++;
                    return true;
                }
                if (EXP())
                {
                    return true;
                }
                
                else return false;
            }
            else if (EXP())
            {
                return true;
            }
            else if (tokenStream[ind].CP == "char" || tokenStream[ind].CP == "string" || tokenStream[ind].CP == "float")
            {
                ind++;
                return true;
            }
            else return false;

        }


        bool COND_C1()
        {
            if (tokenStream[ind].CP == "RO")
            {
                ind++;
                if (COND_ID())
                {
                    if (COND_C1())
                    {
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "LO"  || tokenStream[ind].CP == ":" || tokenStream[ind].CP == ")" || tokenStream[ind].CP == ";")
            {
                return true;
            }
            else return false;
        }

        bool COND_C() //C
        {
            if (COND_ID())
            {
                if (COND_C1())
                {
                    return true;
                }
                else return false;
            }
            else return false;
        }

        bool COND1() //C`
        {
            if (tokenStream[ind].CP == "LO")
            {
                ind++;
                if (COND_C())
                {
                    if (COND_C1())
                    {
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if ( tokenStream[ind].CP == ":" || tokenStream[ind].CP == ")" || tokenStream[ind].CP == ";")
            {
                return true;
            }
            else return false;
        }
        public bool COND()
        {
            if (COND_C())
            {
                if (COND1())
                {
                    return true;
                }
                else return false;
            }
            else return false;
        }



        //
        //
        // C_PARAM
        //
        //
        bool INDX()
        {
            if (EXP())
            {
                return true;
            }
            else return false;
        }
        bool P_DIM()
        {
            if (tokenStream[ind].CP == "[")
            {
                ind++;
                if (INDX())
                {
                    if (tokenStream[ind].CP == "]")
                    {
                        ind++;
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "DIVMUL" || tokenStream[ind].CP == "ADDSUB" || tokenStream[ind].CP == "," || tokenStream[ind].CP == ")" || tokenStream[ind].CP == "::")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        bool P_F3()
        {
            if (tokenStream[ind].CP == "::")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    if (P_F2())
                    {
                        if (P_F3())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "DIVMUL" || tokenStream[ind].CP == "ADDSUB" || tokenStream[ind].CP == "," || tokenStream[ind].CP == ")")
            {
                return true;
            }
            else return false;
        }
        bool P_F2()
        {
            if (tokenStream[ind].CP == "(")
            {
                ind++;
                if (C_PARAM())
                {
                    if (tokenStream[ind].CP == ")")
                    {
                        ind++;
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "[")
            {
                ind++;
                if (INDX())
                {
                    if (tokenStream[ind].CP == "]")
                    {
                        ind++;
                        if (P_DIM())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "::" || tokenStream[ind].CP == ")")
            {
                return true;
            }
            else return false;
        }

        bool P_F1()
        {
            if (tokenStream[ind].CP == "(")
            {
                ind++;
                if (C_PARAM())
                {
                    if (tokenStream[ind].CP == ")")
                    {
                        ind++;
                        if (P_F3())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "INCDEC")
            {
                ind++;
                return true;
            }
            else if (tokenStream[ind].CP == "[")
            {
                ind++;
                if (INDX())
                {
                    if (tokenStream[ind].CP == "]")
                    {
                        ind++;
                        if (P_DIM())
                        {
                            if (P_F3())
                            {
                                return true;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "::")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    if (P_F2())
                    {
                        if (P_F3())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "DIVMUL" || tokenStream[ind].CP == "ADDSUB" || tokenStream[ind].CP == "," || tokenStream[ind].CP == ")")
            {
                return true;
            }
            else return false;
        }
        bool P_F()
        {
            if (tokenStream[ind].CP == "(")
            {
                ind++;
                if (COND())
                {
                    if (tokenStream[ind].CP == ")")
                    {
                        ind++;
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "ID")
            {
                ind++;
                if (P_F1())
                {
                    return true;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "INCDEC")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    return true;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "int" || tokenStream[ind].CP == "float")
            {
                ind++;
                return true;
            }
            else return false;
        }

        bool P_T1()
        {
            if (tokenStream[ind].CP == "DIVMUL")
            {
                ind++;
                if (P_F())
                {
                    if (P_T1())
                    {
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "ADDSUB" || tokenStream[ind].CP == "," || tokenStream[ind].CP == ")")
            {
                return true;
            }
            else return false;
        }

        bool P_T()
        {
            if (P_F())
            {
                if (P_T1())
                {
                    return true;
                }
                else return false;
            }
            else return false;
        }

        bool P_E1()
        {
            if (tokenStream[ind].CP == "ADDSUB")
            {
                ind++;
                if (P_T())
                {
                    if (P_E1())
                    {
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "," || tokenStream[ind].CP == ")")
            {
                return true;
            }
            else return false;
        }
        bool C_MULTIPARAM()
        {
            if (tokenStream[ind].CP == ",")
            {
                ind++;
                if (P_T())
                {
                    if (P_E1())
                    {
                        return true;
                    }
                    else return false;
                }
                else if (tokenStream[ind].CP == "string" || tokenStream[ind].CP == "char")
                {
                    ind++;
                    if (C_MULTIPARAM())
                    {
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == ")")
            {
                return true;
            }
            else return false;
        }
        bool C_PARAM()
        {
            if (P_T())
            {
                if (P_E1())
                {
                    if (C_MULTIPARAM())
                    {
                        return true;
                    }
                    else return false;
//                    return true;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "string" || tokenStream[ind].CP == "char")
            {
                ind++;
                if (C_MULTIPARAM())
                {
                    return true;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == ")")
            {
                return true;
            }
            else return false;
        }

        //
        //
        //EXP
        //
        //
        bool exp_DIM()
        {
            if (tokenStream[ind].CP == "[")
            {
                ind++;
                if (INDX())
                {
                    if (tokenStream[ind].CP == "]")
                    {
                        ind++;
                        if (exp_DIM())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "DIVMUL" || tokenStream[ind].CP == "ADDSUB" || tokenStream[ind].CP == ")" || tokenStream[ind].CP == ":" || tokenStream[ind].CP == ";" || tokenStream[ind].CP == "RO" || tokenStream[ind].CP == "::" || tokenStream[ind].CP == "LO")
            {
                return true;
            }
            else return false;
        }
        bool exp_F3()
        {
            if (tokenStream[ind].CP == "::")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    if (exp_F2())
                    {
                        if (exp_F3())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "DIVMUL" || tokenStream[ind].CP == "ADDSUB" || tokenStream[ind].CP == ")" || tokenStream[ind].CP == "]" || tokenStream[ind].CP == "RO" || tokenStream[ind].CP == "LO" || tokenStream[ind].CP == ";" || tokenStream[ind].CP == ":")
            {
                return true;
            }
            else return false;
        }

        bool exp_F2()
        {
            if (tokenStream[ind].CP == "(")
            {
                ind++;
                if (C_PARAM())
                {
                    if (tokenStream[ind].CP == ")")
                    {
                        ind++;
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "[")
            {
                ind++;
                if (INDX())
                {
                    if (tokenStream[ind].CP == "]")
                    {
                        ind++;
                        if (exp_DIM())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "DIVMUL" || tokenStream[ind].CP == ";" || tokenStream[ind].CP == "ADDSUB" || tokenStream[ind].CP == ")" || tokenStream[ind].CP == "::" || tokenStream[ind].CP == "]" || tokenStream[ind].CP == "RO" || tokenStream[ind].CP == "LO" || tokenStream[ind].CP == ":")
            {
                return true;
            }
            else return false;

        }

        bool exp_F1()
        {
            if (tokenStream[ind].CP == "(")
            {
                ind++;
                if (C_PARAM())
                {
                    if (tokenStream[ind].CP == ")")
                    {
                        ind++;
                        if (exp_F3())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "INCDEC")
            {
                ind++;
                return true;
            }
            else if (tokenStream[ind].CP == "[")
            {
                ind++;
                if (INDX())
                {
                    if (tokenStream[ind].CP == "]")
                    {
                        ind++;
                        if (exp_DIM())
                        {
                            if (exp_F3())
                            {
                                return true;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "::")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    ind++;
                    if (exp_F2())
                    {
                        if (exp_F3())
                        {
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (tokenStream[ind].CP == "DIVMUL" || tokenStream[ind].CP == ";" || tokenStream[ind].CP == "ADDSUB" || tokenStream[ind].CP == ")" ||  tokenStream[ind].CP == "]" || tokenStream[ind].CP == "RO" || tokenStream[ind].CP == "LO" || tokenStream[ind].CP == ":")
            {
                return true;
            }
            else { return false; }
        }

        bool exp_F()
        {
            if (tokenStream[ind].CP == "(")
            {
                ind++;
                if (COND())
                {
                    if (tokenStream[ind].CP == ")")
                    {
                        ind++;
                        return true;
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "ID")
            {
                ind++;
                if (exp_F1())
                {
                    return true;
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "INCDEC")
            {
                ind++;
                if (tokenStream[ind].CP == "ID")
                {
                    return true;
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "int" || tokenStream[ind].CP == "float")
            {
                ind++;
                return true;
            }
            else { return false; }
        }


        bool exp_T1()
        {
            if (tokenStream[ind].CP == "DIVMUL")
            {
                ind++;
                if (exp_F())
                {
                    if (exp_T1())
                    {
                        return true;
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == "ADDSUB"  || tokenStream[ind].CP == ";" || tokenStream[ind].CP == ")"  || tokenStream[ind].CP == "]" || tokenStream[ind].CP == "RO" || tokenStream[ind].CP == "LO" || tokenStream[ind].CP == ":")
            {
                return true;
            }
            else { return false; }
        }

        bool exp_T()
        {
            if (exp_F())
            {
                if (exp_T1())
                {
                    return true;
                }
                else { return true; }
            }
            else { return false; }
        }

        bool EXP1()
        {
            if (tokenStream[ind].CP == "ADDSUB")
            {
                ind++;
                if (exp_T())
                {
                    if (EXP1())
                    {
                        return true;
                    }
                    else { return false; }
                }
                else { return false; }
            }
            else if (tokenStream[ind].CP == ")" || tokenStream[ind].CP == ";" || tokenStream[ind].CP == "]" || tokenStream[ind].CP == "RO" || tokenStream[ind].CP == "LO" || tokenStream[ind].CP == ":")
            {
                return true;
            }
            else { return false; }
        }

        public bool EXP()
        {
            if (exp_T())
            {
                if (EXP1())
                {
                    return true;
                }
                else { return false; }
            }
            else { return false; }
        }


    }
}
